package
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.NetworkInfo;
	import flash.net.NetworkInterface;
	
	import mx.core.UIComponent;
	
	
	/**
	 *ネットにつながってるかみるあれ．赤と青のあれ． 
	 * 置くだけでOK
	 * @author 完
	 * 
	 */
	public class NetStateChecker extends UIComponent
	{
		
		private var netShapeOff:Shape = new Shape();//ネットワーク状態
		private var netShapeOn:Shape = new Shape();
		
		[Embed(source='images/netOff.png')] private var netOffPng:Class;
		private var bitmapOff:Bitmap = new netOffPng();
		[Embed(source='images/netOn.png')] private var netOnPng:Class;
		private var bitmapOn:Bitmap = new netOnPng();
		
		public function NetStateChecker()
		{
			super();
				

			netShapeOff.graphics.beginBitmapFill(bitmapOff.bitmapData);
			netShapeOff.graphics.drawRect(0,0,80,80);
			netShapeOff.graphics.endFill();
			this.addChild(netShapeOff);
			netShapeOff.x = 0;
			netShapeOff.y = 0;

			netShapeOn.graphics.beginBitmapFill(bitmapOn.bitmapData);
			netShapeOn.graphics.drawRect(0,0,80,80);
			netShapeOn.graphics.endFill();
			this.addChild(netShapeOn);
			netShapeOn.x = 0;
			netShapeOn.y = 0;
			netShapeOff.visible = false;
			netShapeOn.visible = false;
			
	
			if (NetworkInfo.isSupported)
			{
				NetworkInfo.networkInfo.addEventListener(Event.NETWORK_CHANGE, onNetworkChange);
				networkCheck();
			}
		}
		
		
		private function onNetworkChange(evt:Event):void
		{
			networkCheck();
		}
		
		private function networkCheck():void
		{
			var isCheck:Boolean = false;
			if (NetworkInfo.isSupported)
			{
				//NetworkInfoを参照します。
				var networkinfo:NetworkInfo = NetworkInfo.networkInfo;
				//NetworkInfoの中に複数のNetworkInterfaceが含まれていて個別に調べます。
				for each (var networkinterface:NetworkInterface in networkinfo.findInterfaces())
				{
					//通信可能なインターフェイスがある場合はそのインターフェイスの情報を表示します。
					if (networkinterface.active)
					{
						isCheck = true;
					}
				}
			}
			if (isCheck == true)
			{
				netShapeOn.visible = true;
				netShapeOff.visible = false;
			}
			else
			{
				netShapeOn.visible = false;
				netShapeOff.visible = true;
			}
		}
		
		
	}
}